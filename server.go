package main

import (
	"encoding/binary"
	"flag"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sort"
	"strings"
	"sync"
	"text/template"

	"github.com/gorilla/websocket"
)

type keyPress struct {
	Key  string `json:"key"`
	Down bool   `json:"down"`
}
type client struct {
	Conn *websocket.Conn
	Keys []string
}

var indexHtml, _ = ioutil.ReadFile("index.html")
var homeTemplate = template.Must(template.New("").Parse(string(indexHtml)))
var addr = flag.String("addr", "localhost:8080", "http service address")
var upgrader = websocket.Upgrader{} // use default options

var keyCodes map[string]byte = map[string]byte{
	"ESCAPE":     1,
	"1":          2,
	"2":          3,
	"3":          4,
	"4":          5,
	"5":          6,
	"6":          7,
	"7":          8,
	"8":          9,
	"9":          10,
	"0":          11,
	"-":          12,
	"=":          13,
	"!":          2,
	"@":          3,
	"#":          4,
	"$":          5,
	"%":          6,
	"^":          7,
	"&":          8,
	"*":          9,
	"(":          10,
	")":          11,
	"_":          12,
	"+":          13,
	"BACKSPACE":  14,
	"TAB":        15,
	"Q":          16,
	"W":          17,
	"E":          18,
	"R":          19,
	"T":          20,
	"Y":          21,
	"U":          22,
	"I":          23,
	"O":          24,
	"P":          25,
	"[":          26,
	"]":          27,
	"{":          26,
	"}":          27,
	"ENTER":      28,
	"CONTROL":    29,
	"A":          30,
	"S":          31,
	"D":          32,
	"F":          33,
	"G":          34,
	"H":          35,
	"J":          36,
	"K":          37,
	"L":          38,
	";":          39,
	"'":          40,
	"`":          41,
	":":          39,
	"\"":         40,
	"~":          41,
	"SHIFT":      42,
	"\\":         43,
	"|":          43,
	"Z":          44,
	"X":          45,
	"C":          46,
	"V":          47,
	"B":          48,
	"N":          49,
	"M":          50,
	",":          51,
	".":          52,
	"/":          53,
	"<":          51,
	">":          52,
	"?":          53,
	"ALT":        56,
	" ":          57,
	"CAPSLOCK":   58,
	"F1":         59,
	"F2":         60,
	"F3":         61,
	"F4":         62,
	"F5":         63,
	"F6":         64,
	"F7":         65,
	"F8":         66,
	"F9":         67,
	"F10":        68,
	"F11":        87,
	"F12":        88,
	"HOME":       102,
	"ARROWUP":    103,
	"PAGEUP":     104,
	"ARROWLEFT":  105,
	"ARROWRIGHT": 106,
	"END":        107,
	"ARROWDOWN":  108,
	"PAGEDOWN":   109,
	"INSERT":     110,
	"DELETE":     111,
}
var keyNames []string
var clients []client
var clientsMutex sync.Mutex

func keyCode(name string, me *client) byte {
	name = strings.ToUpper(name)
	v, found := keyCodes[name]
	if !found {
		log.Printf("not found: %s", name)
		return 0
	}

	// TODO: nooooo Keys is empty
	return v
	for _, allowed := range me.Keys {
		if allowed == name {
			return v
		}
	}

	log.Printf("nope: %s from %v", name, me.Conn.RemoteAddr())
	return 0
}

func emitKey(press *keyPress, me *client) {
	var direction byte

	if press.Down {
		direction = 0x01
	} else {
		direction = 0x00
	}

	code := keyCode(press.Key, me)

	binary.Write(os.Stdout, binary.LittleEndian, []byte{code, direction})
}

func distributeKeys() {
	if len(clients) == 0 {
		log.Printf("no clients to distribute keys to")
		return
	}

	clientsMutex.Lock()

	for i, _ := range clients {
		clients[i].Keys = []string{}
	}

	var j int = 0;
	for _, keyName := range keyNames {
		if len(clients) == j {
			j = 0;
		}
		log.Printf("giving key %s to %d", keyName, j)
		clients[j].Keys = append(clients[j].Keys, keyName)
		j++;
	}
	clientsMutex.Unlock()

	for _, c := range clients {
		if err := c.Conn.WriteJSON(c.Keys); err != nil {
			log.Println("write:", err)
		}
	}
}

func hello(c *websocket.Conn) *client {
	log.Printf("hello client %v", c.RemoteAddr())
	me := client{
		Conn: c,
	}
	clientsMutex.Lock()
	clients = append(clients, me)
	clientsMutex.Unlock()
	distributeKeys()

	return &me
}

func bye(c *websocket.Conn) {
	log.Printf("bye client %v", c.RemoteAddr())
	clientsMutex.Lock()
	for i, client := range clients {

		if client.Conn == c {
			c.Close()
			clients = append(clients[:i], clients[i+1:]...)
		}
	}
	clientsMutex.Unlock()
	distributeKeys()
}

func webSocket(w http.ResponseWriter, r *http.Request) {
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	}
	defer bye(c)
	me := hello(c)
	for {
		press := &keyPress{}
		err := c.ReadJSON(press)
		if err != nil {
			log.Println("read:", err)
			break
		}
		log.Printf("recv: %v", press)
		emitKey(press, me)
	}
}

func home(w http.ResponseWriter, r *http.Request) {
	homeTemplate.Execute(w, "ws://"+r.Host+"/ws")
}

func main() {
	keyNames = make([]string, len(keyCodes))
	i := 0
	for name := range keyCodes {
		keyNames[i] = name
		i++
	}
	sort.Strings(keyNames)

	flag.Parse()
	log.SetFlags(0)
	http.HandleFunc("/ws", webSocket)
	http.HandleFunc("/", home)
	log.Fatal(http.ListenAndServe(*addr, nil))
}
