#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <linux/uinput.h>

void emit(int fd, int type, int code, int val) {
  struct input_event ie;

  ie.type = type;
  ie.code = code;
  ie.value = val;
  ie.time.tv_sec = 0;
  ie.time.tv_usec = 0;

  if (write(fd, &ie, sizeof(ie)) < 0) {
    perror("uinput: emit write failed");
  }
}

void read_keys(int uinput_fd) {
  FILE *fd = freopen(NULL, "rb", stdin);
  char keys[2];

  while (fread(keys, sizeof(keys), 1, fd) == 1) {
    printf("uinput: read key %d with action %s\n", (int)keys[0], (int)keys[1] ? "down" : "up");
    emit(uinput_fd, EV_KEY, (int)keys[0], (int)keys[1]);
    emit(uinput_fd, EV_SYN, SYN_REPORT, 0);
  }
}

int main(void) {
  struct uinput_setup usetup;

  int fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK);

  ioctl(fd, UI_SET_EVBIT, EV_KEY);
  /* Basically every key on an AT keyboard */
  for (unsigned char i = 1; i < 255; i++) {
    ioctl(fd, UI_SET_KEYBIT, i);
  }

  memset(&usetup, 0, sizeof(usetup));
  usetup.id.bustype = BUS_USB;
  usetup.id.vendor = 0x1234; /* sample vendor */
  usetup.id.product = 0x5678; /* sample product */
  strcpy(usetup.name, "Beunen Keyboard");

  ioctl(fd, UI_DEV_SETUP, &usetup);
  ioctl(fd, UI_DEV_CREATE);

  /* Wait for userspace to get our keyboard connected */
  sleep(1);

  read_keys(fd);

  /* And give userspace time to clean up */
  sleep(1);

  ioctl(fd, UI_DEV_DESTROY);
  close(fd);

  return 0;
}
