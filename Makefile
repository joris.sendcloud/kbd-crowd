.PHONY: all

server: server.go
	go build server.go

uinput: uinput.c
	cc -Wall -o uinput uinput.c

all: uninput server
